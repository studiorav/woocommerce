<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://studiorav.co.uk
 * @since      1.0.0
 *
 * @package    Woo_PaybyBank
 * @subpackage Woo_PaybyBank/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Woo_PaybyBank
 * @subpackage Woo_PaybyBank/includes
 * @author     Woo_PaybyBank Nishshanka <harshana@dayzsolutions.com>
 */
class Woo_PaybyBank_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
