<?php
class WC_PaybyBank_Gateway extends WC_Payment_gateway
{
    /**
     * Class constructor, more about it in Step 3
     */
    public function __construct()
    {
        $this->id = 'paybybank-gateway'; // payment gateway plugin ID
        $this->icon = plugin_dir_url('woo-paybybank') . 'woo-paybybank/assets/paybybank.png';
        $this->has_fields = true;
        $this->method_title = 'Pay by Bank | fumopay';
        $this->method_description = 'Pay by Bank is a secure account-to-account payment facilitated by fumopay. No card data entry, simply select your bank, authorise and pay. Powered by fumopay®';

        // gateways can support subscriptions, refunds, saved payment methods
        $this->supports = array(
            'products',
            'refunds'
        );
        $this->init_form_fields();

        $this->init_settings();
        $this->title = $this->get_option('title');
        $this->description = $this->get_option('description');
        $this->enabled = $this->get_option('enabled');
        $this->testmode = $this->get_option('testmode');

        $this->template_id = $this->get_option('template_id');

        if($this->testmode != 'yes'){
            $this->apiURL = 'https://fumopay.app';
            $this->profile_key = $this->get_option('profile_key');
            $this->secret_key = $this->get_option('secret_key');
        }else{
            $this->apiURL = 'https://fumopay.dev';
            $this->profile_key = $this->get_option('test_profile_key');
            $this->secret_key = $this->get_option('test_secret_key');
        }
        // $this->description_for_customer_side = $this->get_option('description_for_customer_side');
        $this->order_button_text = __('Pay by Bank', 'woocommerce');

        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));

        // webhook URL https://websiteurl.com/wc-api/paybybank-payment-response
        add_action('woocommerce_api_paybybank_payment_response', array($this, 'paybybank_webhook'));

        $this->logger = wc_get_logger();
    }

    /**
     * Plugin options, we deal with it in Step 3 too
     */
    public function init_form_fields()
    {
        $this->form_fields = array(
            'enabled' => array(
                'title'       => 'Enable/Disable',
                'label'       => 'Enable Pay by Bank Gateway',
                'type'        => 'checkbox',
                'description' => '',
                'default'     => 'no'
            ),
            'title' => array(
                'title'       => 'Title',
                'type'        => 'text',
                'description' => 'This controls the title which the user sees during checkout.',
                'default'     => $this->method_title,
                'desc_tip'    => true,
            ),
            'description' => array(
                'title'       => 'Description',
                'type'        => 'textarea',
                'description' => 'This controls the description which the user sees during checkout.',
                'default'     => $this->method_description,
            ),
            'testmode' => array(
                'title'       => 'Test mode',
                'label'       => 'Enable Test Mode',
                'type'        => 'checkbox',
                'description' => 'Place the payment gateway in test mode using test API keys.',
                'default'     => 'yes',
                'desc_tip'    => true,
            ),
            'test_profile_key' => array(
                'title'       => 'Test Profile Key',
                'type'        => 'text'
            ),
            'test_secret_key' => array(
                'title'       => 'Test Secret Key',
                'type'        => 'password',
            ),
            'profile_key' => array(
                'title'       => 'Live Profile Key',
                'type'        => 'text'
            ),
            'secret_key' => array(
                'title'       => 'Live Secret Key',
                'type'        => 'password'
            ),
            // 'description_for_customer_side' => array(
            //     'title'       => 'Description for customer\'s app',
            //     'type'        => 'textarea',
            //     'description' => 'This will display as the description which the user sees during his payment credeentials screen. Usable tags are {{order_id}}, {{site_url}}',
            //     'default'     => 'Payment for {{order_id}} - {{site_url}}',
            // ),
        );
    }

    public function findReplace($string, $find, $replace)
    {
        if (preg_match("/[a-zA-Z\_]+/", $find)) {
            return (string) preg_replace("/\{\{(\s+)?($find)(\s+)?\}\}/", $replace, $string);
        } else {
            throw new \Exception("Find statement must match regex pattern: /[a-zA-Z]+/");
        }
    }

    /**
     * You will need it if you want your custom credit card form, Step 4 is about it
     */
    public function payment_fields()
    {
        echo '<style>
        label[for="payment_method_paybybank-gateway"] img {
            max-width: 140px;
            margin-left: 10px;
        }
        
        label[for="payment_method_paybybank-gateway"] {
            display: flex;
            align-items: center;
        }</style>';
        
        if ($this->description) {
            if ($this->testmode == 'yes') {
                $this->description .= '<br><strong style="color:red">Pay by Bank is on test mode.</strong>';
            }
            echo wpautop(wp_kses_post($this->description));
        }
    }

    /*
              * Fields validation, more in Step 5
             */
    public function validate_fields()
    {
        if (empty($_POST['billing_first_name'])) {
            wc_add_notice('First name is required!', 'error');
            return false;
        }
        return true;
    }

    /*
             * We're processing the payments here, everything about it is in Step 5
             */
    public function process_payment($order_id)
    {
        global $woocommerce;

        // we need it to get any order detailes
        $order = wc_get_order($order_id);

        $profileKey = $this->profile_key;
        $secretKey = $this->secret_key;

        // Process Description
        // $description = $this->findReplace($this->description_for_customer_side, 'order_id', $order_id);
        $description = "Payment for Order #" . $order_id . ' - ' . site_url();

        $customer = $order->get_customer_id();
        $amount = $order->get_total() * 100; //Convert to minor units
        $reference = 'order_' . $order_id;

        $strToHash = implode('', [get_woocommerce_currency(), $description, $profileKey, $reference, $secretKey]);
        $hashed = hash("sha512", $strToHash, true);
        $signature = base64_encode($hashed);

        update_post_meta( $order_id, 'signature', $signature );
        $thank_you_page = $order->get_checkout_order_received_url();
        $payload['headers'] = array('Content-Type: application/json');
        $payload['body'] = json_encode(array(
            'merchant_profile' => array('profile_key' => $profileKey),
            'currency' => get_woocommerce_currency(),
            'description' => $description,
            'ext_reference' => $reference,
            'ext_customer' => 'Customer Email: ' . $order->get_billing_email(),
            'amount' => $amount,
            'age' => 1, //How many hours the transaction stays active. This is an optional field with a default of 1 hour.
            'redirect_url' => $thank_you_page,
            'signature' => $signature,
            'email' => $order->get_billing_email(),
            'platform' => 'woocommerce'
        ));

        setcookie('paybybank_order_id', $order_id, time() + 3600, '/');

        $response = wp_remote_post( $this->apiURL . '/transaction/pay/', $payload);
        wc_get_logger()->info( 'URL ' . $this->apiURL . '/transaction/pay/' . ' | Payload ' . json_encode($payload), array( 'source' => 'paybybank' ) );
        wc_get_logger()->info( 'Response from Pay by Bank' . json_encode($response['body']) , array( 'source' => 'paybybank' ) );

        if (!is_wp_error($response)) {
            $body = json_decode($response['body']);

            // Response Codes: 1: Pending/OK, 2: Declined, 3: Error, 11: Paid, 12: Failed
            if ($body->result == '11') {
                // we received the payment
                $order->payment_complete();
                $order->reduce_order_stock();
                // some notes to customer (replace true with false to make it private)
                $order->add_order_note('Hey, your order is paid! Thank you!', true);
                // Empty cart
                $woocommerce->cart->empty_cart();
                // Redirect to the thank you page
                return array(
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order)
                );
            } elseif ($body->result == '1') { //Pending payment - Redirect customer to the pay screen
                $order->update_status('on-hold', 'Payment is pending with Pay by Bank! Order will get updated once payment is completed.');
                // Redirect to the payment page
                return array(
                    'result' => 'success',
                    'redirect' => $this->apiURL . '/process/'.$profileKey.'/' . $body->transaction_id
                );
            } else {
                wc_add_notice('Please try again.', 'error');
                return;
            }
        } else {
            $order->update_status('cancelled', 'There was a problem redirecting to the payment gateway. If this problem persists please contact Customer Services.');
            throw new Exception(__('We are currently experiencing problems trying to connect to this payment gateway. Sorry for the inconvenience.', 'woo-paybybank'));
            wc_add_notice('Connection error.', 'error');
            return;
        }
    }

    /*
             * In case you need a webhook, like PayPal IPN etc
             */
    public function paybybank_webhook()
    {
        $data = json_decode(file_get_contents('php://input'), true);

        wc_get_logger()->info( 'Pay by Bank Webhook POST: ' . json_encode($data) , array( 'source' => 'paybybank' ) );

        if(!isset($data['reference'])){
            return false;
        }

        $ref = $data['reference'];
        if(isset($data['transaction_id'])){
            $order_id = str_replace('order_', '', $ref);
            $order = wc_get_order($order_id);

            if($data['result'] == '11'){
                wc_get_logger()->info( 'Pay by Bank Webhook - Payment Completed, Order ID:' . $order_id , array( 'source' => 'paybybank' ) );
                $order->add_order_note('Payment successful via Pay by Bank - transaction id: ' . $data['transaction_id']);
                $order->payment_complete();
                update_post_meta( $order_id, 'transaction_id', $data['transaction_id'] );
                $order->set_transaction_id($data['transaction_id']);
                $order->save();
                $order->reduce_order_stock();
                update_option('webhook_debug', $data);
            }elseif($data['result'] == '3'){
                wc_get_logger()->info( 'Pay by Bank Webhook - Payment Error, Order ID:' . $order_id , array( 'source' => 'paybybank' ) );
            $order->update_status('cancelled', 'There was an error with the payment.');
            }elseif($data['result'] == '12'){
                wc_get_logger()->info( 'Pay by Bank Webhook - Payment Failed, Order ID:' . $order_id , array( 'source' => 'paybybank' ) );
                $order->update_status('cancelled', 'Payment failed.');
            }else{
                wc_get_logger()->info( 'Pay by Bank Webhook - Payment unknown error, Order ID:' . $order_id , array( 'source' => 'paybybank' ) );  
            }
        }elseif($data['refund_id']){
            $order_id = str_replace('REF-order_', '', $data['reference']);
            $order = wc_get_order($order_id);
            $order->add_order_note('Order refunded. Refund ID: ' . $data['refund_id']);

            // $order->update_status('wc-refunded', 'Order refunded. Refund ID: ' . $data['refund_id']);
            wc_get_logger()->info( 'Pay by Bank Refund Webhook - ' . json_encode($data), array( 'source' => 'paybybank' ) );
        }
    }

    public function process_refund( $order_id, $refundAmount = null, $reason = '' ) {
        $order = new WC_Order( $order_id );

        if(!$this->can_refund_order($order)) {
            wc_get_logger()->info('Refund Failed: No transaction ID', array( 'source' => 'paybybank' ));
            return new WP_Error('error', __('Refund Failed: No transaction ID', 'pay-via-barion-for-woocommerce'));
        }

        try{
            $transaction_id = $order->get_transaction_id();
            // $transaction_id = get_post_meta($order_id, 'transaction_id', true);

            $transaction = $this->get_transaction($order_id, $transaction_id);
            wc_get_logger()->info('Get Transaction: '.$transaction_id . ' AND ' . $transaction, array( 'source' => 'paybybank' ));
            $transaction = json_decode($transaction);

            if($transaction->transaction->merchant_account->bank_id == 11){ //Cashplus - Not supported to refunds
                $adminnotice = new WC_Admin_Notices();
                $adminnotice->add_custom_notice("Cashplus","<div>Cashplus is not supported for refunding orders.</div>");
                $adminnotice->output_custom_notices();
                return false;
            }

            $amount =  number_format(floatval($refundAmount)*100, 0, '', '');
            $currency = $order->get_currency();
            $refund = $this->post_refund_data($order_id, $transaction_id, $amount, $currency, $transaction);
            wc_get_logger()->info('Posted Refund: '.$refund, array( 'source' => 'paybybank' ));
            $refund = json_decode($refund);

            if($refund && $refund->result == 1){
                $order->add_order_note(sprintf(__('Refund authorisation is required (valid only 5mins): %s', 'paybybank' ), wc_price($refundAmount), '<a href="' . $refund->url . '">Click here to authorize the refund.</a>'));
                return true;
            }

        } catch (\Exception $e) {
            return new \WP_Error($e->getCode(), $e->getMessage(), isset($e->data) ? $e->data : '');
        }
        return false;
    }

    public function get_transaction($order_id, $transaction_code){
        $order = wc_get_order($order_id);
        $profile_key = $this->profile_key;
        $secret_key = $this->secret_key;
        $signed_date_header =  date('Y-m-d\TH:i:sP');
        // $transaction_code = '8362072e-622d-46a5-a047-8b1e1d09e666'; //Test purpose
        $strToHash = implode('',[$profile_key, $transaction_code, $signed_date_header, $secret_key]);
        $hashed = hash("sha512", $strToHash, true);
        $signature = base64_encode($hashed);

        $args = array(
            'headers' => array(
                'X-Fumo-Sign-Date' => $signed_date_header,
                'X-Fumo-Signature' => $signature
            )
        );

        $response = wp_remote_get($this->apiURL . '/transaction/get/'.$profile_key.'/'.$transaction_code, $args);
        // wc_get_logger()->info('Transaction URL: '.$this->apiURL . '/transaction/get/'.$profile_key.'/'.$transaction_code, array( 'source' => 'paybybank' ));
        // wc_get_logger()->info('Transaction curl: '.$response, array( 'source' => 'paybybank' ));

        if( is_wp_error( $response ) ) {
            wc_get_logger()->info('Refund Failed: ' . json_encode($response), array( 'source' => 'paybybank' ));
            new \WP_Error($response->getCode(), json_encode($response->errors), '');

            return false;
        }
        
        return wp_remote_retrieve_body( $response );
    }

    public function post_refund_data($order_id, $transaction_id, $amount, $currency, $transaction){
        $profileKey = $this->profile_key;
        $secretKey = $this->secret_key;
        // $transaction_id = '8362072e-622d-46a5-a047-8b1e1d09e666';
        $strToHash = implode('',[$transaction_id, $profileKey, 'GBP', $amount, $secretKey]);
        $hashed = hash("sha512", $strToHash, true);
        $signature = base64_encode($hashed);
        // wc_get_logger()->info('Transaction data for refund : ' . json_encode($transaction), array( 'source' => 'paybybank' ));
        $body = array(
            'profile_key' => $profileKey,
            'amount' => intval($amount),
            'currency' => $currency,
            'transaction_id' => $transaction_id,
            'reference' => 'order_' . $order_id,
            'to_account' => array(
                'name' => $transaction->transaction->account_name,
                'scheme' => $transaction->transaction->account_scheme,
                'number' => $transaction->transaction->account_number,
                'bank_id' => $transaction->transaction->bank->id
            ),
            'signature' => $signature,
            'platform' => 'woocommerce'
        );

        wc_get_logger()->info('Body : ' . json_encode($body), array( 'source' => 'paybybank' ));

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->apiURL . '/transaction/refund/' . $transaction_id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>json_encode($body),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        // echo $response;
        // $response = wp_remote_post($this->apiURL . '/transaction/refund/' . $transaction_id, array('body' => ($body)));
        wc_get_logger()->info('Refund: ' . json_encode($response), array( 'source' => 'paybybank' ));

        if( is_wp_error( $response ) ) {
            wc_get_logger()->info('Refund Failed: ' . json_encode($response), array( 'source' => 'paybybank' ));

            return false;
        }

        return $response;
        // return wp_remote_retrieve_body( $response );
    }
}
