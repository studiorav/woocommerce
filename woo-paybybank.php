<?php


/**
 *
 * @link              https://studiorav.co.uk
 * @since             1.0.0
 * @package           Woo_PaybyBank
 *
 * @wordpress-plugin
 * Plugin Name:       Pay by Bank - fumopay
 * Plugin URI:        https://studiorav.co.uk
 * Description:       OpenBanking solution for Pay by Bank - Woocommerce integration.
 * Version:           1.0.0
 * Author:            Studiorav
 * Author URI:        https://studiorav.co.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woo-paybybank
 * Domain Path:       /languages

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WOO_PAYBYBANK_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woo-paybybank-activator.php
 */
function activate_woo_paybybank() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woo-paybybank-activator.php';
	Woo_PaybyBank_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woo-paybybank-deactivator.php
 */
function deactivate_woo_paybybank() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woo-paybybank-deactivator.php';
	Woo_PaybyBank_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_woo_paybybank' );
register_deactivation_hook( __FILE__, 'deactivate_woo_paybybank' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woo-paybybank.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_woo_paybybank() {

	$plugin = new Woo_PaybyBank();
	$plugin->run();

}
run_woo_paybybank();

add_action('template_redirect', 'paybybank_success_redirect');
function paybybank_success_redirect() {
	if (isset($_GET['paybybank-pay'])){
		wc_get_logger()->info( 'Pay by Bank redirect parameters - ' . $_GET['paybybank-pay'] , array( 'source' => 'paybybank' ) );
	}

	if (isset($_GET['paybybank-pay']) and $_GET['paybybank-pay'] == "success") {
		if (isset($_COOKIE['paybybank_order_id'])) {
			$order = wc_get_order($_COOKIE['paybybank_order_id']);
			$redirectTo = $order->get_checkout_order_received_url();
			wc_get_logger()->info( 'Pay by Bank redirect url - Success, ' . $redirectTo , array( 'source' => 'paybybank' ) );

			wp_redirect( $redirectTo ); 
			exit; 
		}else{
			wc_get_logger()->info( 'Pay by Bank redirect url - no paybybank order id - redirecting to home' , array( 'source' => 'paybybank' ) );
			wp_redirect( site_url() ); 
			exit; 
		}
	}elseif(isset($_GET['paybybank-pay']) and $_GET['paybybank-pay'] == "error"){
		if (isset($_COOKIE['paybybank_order_id'])) {
			$order = wc_get_order($_COOKIE['paybybank_order_id']);
			$order->update_status( 'cancelled', 'There was an error with the transaction for order #' . $order->get_id() );
			wc_add_notice( 'There was an error with the transaction for order #' . $order->get_id(), 'notice' );
			wc_get_logger()->info( 'Pay by Bank redirect url - Error - Redirecting to Cart with notice' , array( 'source' => 'paybybank' ) );

			wp_redirect( wc_get_cart_url() ); 
			exit;  
		}else{
			wc_get_logger()->info( 'Pay by Bank redirect url - Erro, no Pay by Bank order id - redirecting to home' , array( 'source' => 'paybybank' ) );
			wp_redirect( site_url() ); 
		}  
	}else{
		return;
